angular.module('myApp.services', [])
  .factory('apiService', ['$http', '$cookies', function($http, $cookies) {

    var urlBase = 'http://162.243.61.116:1337/';
    var dataFactory = {};

    var getConfig = function() {
      return {
        headers: {
        }
      }
    };


    dataFactory.login = function(loginData) {
      return $http.post(urlBase + 'auth/login', loginData);
    };

    dataFactory.addEvent = function(event) {
      return $http.post(urlBase + 'event/', event);
    };

    dataFactory.getEvents = function() {
      return $http.get(urlBase + 'event');
    };

    dataFactory.deleteEvent = function(id) {
      return $http.delete(urlBase + 'event/' + id);
    };

    dataFactory.cancelEvent = function (id) {
        return $http.post(urlBase + 'event/cancel/' + id);
    };

    dataFactory.undoCancelEvent = function (id) {
        return $http.post(urlBase + 'event/undoCancel/' + id);
    };

    dataFactory.getLocations = function() {
      return $http.get(urlBase + 'location');
    };

    dataFactory.addLocation = function(location) {
      return $http.post(urlBase + 'location/', location);
    };

    dataFactory.deleteLocation = function(id) {
      return $http.delete(urlBase + 'location/' + id);
    };

     dataFactory.getTickets = function() {
      return $http.get(urlBase + 'ticket');
    };

    dataFactory.addTicket = function(ticket){
        return $http.post(urlBase + 'ticket/', ticket);
    };

    dataFactory.deleteTicket = function(id) {
      return $http.delete(urlBase + 'ticket/' + id);
    };

    return dataFactory;
  }]);
