'use strict';

// Declare app level module which depends on views, and components

angular.module('myApp.controllers',[])


angular.module('myApp', [
  'ngRoute',
  'ngResource',
  'ngCookies',
  'myApp.services',
  'myApp.controllers',
  'myApp.version'
  ]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider
	  .when('/login', {
	                templateUrl: 'login/login.html',
	                controller: 'LoginController',
	                controllerAs: 'loginCtrl'
	            })

	  			.when('/home', {
	                templateUrl: 'Home/home.html'
	            })

	            .when('/events', {
	                controller: 'EventController',
	                templateUrl: 'events/event.html',
	                controllerAs: 'EventCtrl'
	            })

	            .when('/register', {
	                controller: 'RegisterController',
	                templateUrl: 'register/register.html',
	                controllerAs: 'regCtrl'
	            })

	            .when('/location', {
	                controller: 'LocationController',
	                templateUrl: 'location/location.html',
	                controllerAs: 'locCtrl'
	            })

	            .when('/tickets', {
	                controller: 'TicketsController',
	                templateUrl: 'tickets/ticket.html',
	                controllerAs: 'ticketCtrl'
	            })

	  .otherwise({redirectTo: '/login'});
}]);
