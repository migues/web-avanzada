'use strict';

(function() {

  angular.module('myApp.controllers')

  .controller('LoginController', ['apiService', '$location', '$cookies', function(apiService, $location, $cookies) {
    this.credentials = {};

    this.login = function() {
      apiService.login(this.credentials)
        .success(function(response) {
          if (response.token != null) {
            $cookies.put('jwt', response.token);
            $location.path('/home');
          } else {
            console.log("logged in failed");
          }
        })
        .error(function(response) {
          console.log("logged in failed");
        });
    };

  }]);

})();
