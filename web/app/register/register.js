'use strict';
(function () {

  var app = angular.module('myApp.controllers');

  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/register', {
      templateUrl: 'register/register.html',
    });
  }]);

  app.controller('RegisterController', function(){
  	this.credentials = {};

  	this.register = function(){

  	};
  });

  })();