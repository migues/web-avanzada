'use strict';

(function () {


  angular.module('myApp.controllers')


  .controller('EventController', ['apiService','$scope',function(apiService,$scope) {
    $scope.events = [];
    this.event = {};

    this.getEvents = function(){
      apiService.getEvents()
      .success(function(response){

        console.log("success!!!");
        for(var i = 0; i < response.length; i++){
          $scope.events.push(response[i]);
        }
        
      })
      .error(function(error){
        console.log("error get function");
      });

    };

    this.addEvent = function(){

      apiService.addEvent(this.event)
      .success(function(response){
        console.log("success!!!");
        $scope.events.push(response);
      })
      .error(function(error){
        console.log("error add function");
      });

      this.event = {};
    };

    this.deleteEvent = function(event) {
      apiService.deleteEvent(event.id)
        .success(function(response) {
          var index = $scope.events.indexOf(event);
          $scope.events.splice(index,1);
        })
        .error(function(error) {
          console.log("error delete function");
        });
    };

    this.ActiveConvert = function(isActive){
      return isActive === true ? 'Si' : 'No';
    };

    this.isAnActiveEvent = function(event){

      if(event.isActive == true){
        this.cancelEvent(event);
      }
      else{
        this.undoCancelEvent(event);
      }

    };

    this.cancelEvent = function(event){
      apiService.cancelEvent(event.id)
        .success(function(response) {

          var index = $scope.events.indexOf(event);
          $scope.events[index].isActive = false;

          console.log('cancel successfully');
        })
        .error(function(error) {
          console.log("error cancel function");
        });
    };

    this.undoCancelEvent = function(event){
      apiService.undoCancelEvent(event.id)
        .success(function(response) {

          var index = $scope.events.indexOf(event);
          $scope.events[index].isActive = true;

          console.log('undoCancel successfully');
        })
        .error(function(error) {
          console.log("error undoCancel function");
        });
    };

  }]);

})();
