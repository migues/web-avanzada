'use strict';
(function(){

  angular.module('myApp.controllers')

	.controller('TicketsController', ['apiService', '$scope',function(apiService, $scope) {
		$scope.tickets = [];
		this.ticket = {};

		this.getTickets = function(){
      apiService.getTickets()
      .success(function(response){

        console.log("success!!!");
        for(var i = 0; i < response.length; i++){
          $scope.tickets.push(response[i]);
        }
        
      })
      .error(function(error){
        console.log("error get function");
      });

    };

		this.addTicket = function(){
      console.log("try to add");
			apiService.addTicket(this.ticket)
			.success(function(response){
	        	console.log("successfully added ticket");
	      	})
			.error(function(error){
	        	console.log("error in add ticket");
	      	});

	    this.ticket = {};
		};

		this.deteleTicket = function(ticket) {
      apiService.deteleTicket(ticket.id)
        .success(function(response) {
          var index = $scope.tickets.indexOf(ticket);
          $scope.tickets.splice(index,1);
        })
        .error(function(error) {
          console.log("error delete function")
        });
    };
	}]);

})();
