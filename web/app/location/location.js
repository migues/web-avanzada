'use strict';

(function () {


  angular.module('myApp.controllers')


  .controller('LocationController', ['apiService','$scope',function(apiService,$scope) {
    $scope.locations = [];
    this.location = {};

    this.getLocations = function(){
      apiService.getLocations()
      .success(function(response){

        console.log("success!!!");
        for(var i = 0; i < response.length; i++){
          $scope.locations.push(response[i]);
        }
        
      })
      .error(function(error){
        console.log("error get function");
      });

    };

    this.addLocation = function(){
      apiService.addLocation(this.location)
      .success(function(response){
        console.log("success!!!");
        $scope.locations.push(response);
      })
      .error(function(error){
        console.log("error add function");
      });

      this.location = {};
    };

    this.deleteLocation = function(location) {
      apiService.deleteLocation(location.id)
        .success(function(response) {
          var index = $scope.locations.indexOf(location);
          $scope.locations.splice(index,1);
        })
        .error(function(error) {
          console.log("error delete function")
        });
    };
  }]);

})();

