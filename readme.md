# Boletería

Este proyecto es sobre un systema de venta de tickets.

## Integrantes
Miguel Saiz     2010-5916
Jose Richardson 2008-6397
Carlos Grisanti 2009-5558
Héctor Figuereo 2009-7040

## Repositorio

https://bitbucket.org/migues/web-avanzada/overview

## Carpetas

src: Contiene el codigo producido
src/web: El backend basado en angular
src/mobile: El proyecto mobile hecho en cordova/ionic
src/server: El API hecho en SailsJS

## Tecnologias usadas

SailsJS para el API
Angular para el backend web
Ionic+cordova para el cliente movil

## Guia para la instalación del proyecto

Instalar sails e ionic

Para instalar sails ejecutar 
npm -g install sails


## Ejecutar el proyecto

* web-avanzada/server: sails lift
* web-avanzada/web: npm start
* web-avanzada/mobile: ionic serve

Para ingresar a la seccion movil ingresar a http://localhost:8100/#/app/

Para ingresar a la seccion web ingresar a http://localhost:8000/app/#/login

Para hacer llamadas a la API, hacer requests a http://localhost:1337/