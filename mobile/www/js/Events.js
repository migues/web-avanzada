
(function () {

angular.module('starter.controllers')

.controller('EventsCtrl', function(apiService,$scope, $stateParams) {
  $scope.events = [];

  $scope.getEvents = function() {
    apiService.getEvents()
    .success(function(response) {
    	$scope.events = response;
    })
    .error(function(response) {
      console.log("Datos no recibidos")
    });
  };

})

})();