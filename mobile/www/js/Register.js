
(function () {

  angular.module('starter.controllers')

  .controller('RegisterController',function(apiService, $scope, $stateParams){
    
    $scope.user = {};
    
    $scope.addUser = function(){

        apiService.register($scope.user).success(function(response){
          console.log("registered succefully");
        })
        .error(function(response) {
            console.log("Reg failed")
        });

        $scope.user = {};
    };

  })

  .controller('EditController', function($scope, $state, $stateParams, User) {
    $scope.updateUser = function() { 
      $scope.user.$update(function() {
        $state.go('events'); 
      });
    };
  })


  })();