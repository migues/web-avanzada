angular.module('starter.services',[])
    .factory('apiService', ['$http', '$cookies', function($http, $cookies) {

    var urlBase = 'http://162.243.61.116:1337/';
    var dataFactory = {};

    var getConfig = function() {
      return {
        headers: {
        }
      }
    };

    dataFactory.login = function (loginData) {
        return $http.post(urlBase + 'auth/login', loginData);
    };

    dataFactory.getEvents = function () {
        return $http.get(urlBase + 'event');
    };

    dataFactory.getTickets = function () {
        return $http.get(urlBase + 'ticket');
    };

    dataFactory.buying = function(amountBought) {
      return $http.post(urlBase + 'buy', amountBought);
    };

    dataFactory.register = function(regData) {
        return $http.post(urlBase + 'user', regData);
    };

     dataFactory.updateUser = function(regData) {
        return $http.put(urlBase + 'user', regData);
    };

    return dataFactory;
}]);
