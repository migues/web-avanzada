angular.module('starter.controllers')

.controller('AppCtrl', function(apiService,$scope, $ionicModal, $timeout, $http) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    apiService.login($scope.loginData)
    .success(function(response) {
      if (response.token != null) {
        $cookies.put('jwt', response.token);
        $scope.modal.hide();
      } else {
        console.log("logged in failed");
      }
    })
    .error(function(response) {
      console.log("logged in failed");
    });
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Festival Presidente', id: 1 },
    { title: 'Barbarela', id: 2 },
    { title: 'Ricardo Arjona', id: 3 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
