'use strict';
(function () {

  var app = angular.module('starter.controllers');

  app.controller('BuyController',function(apiService){
    
    this.buyer = {};
    
    this.addBuy = function(){

        apiService.buying(this.buyer).success(function(response){
          console.log("bought succefully");
        })
        .error(function(response) {
            console.log("buy failed")
        });

        this.buyer = {};
    };

  });

  })();