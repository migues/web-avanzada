/**
 * LocationController
 *
 * @description :: Server-side logic for managing locations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  addLocation: function(req, res, next) {

    var locObj = {
      name: req.param('name'),
      latitude: req.param('latitude'),
      longitude: req.param('longitude')
    }

    Location.create(locObj, function locationCreated(err, location) {
      if (err) {
        console.log(err);
        req.session.flash = {
          err: err
        }
      } else {
        if (validator.validate(["columns", "rows"], req.body)) {
          location.columns = req.body.columns;
          location.rows = req.body.rows;
          location.save(function() {
            var seats = []
            for (var i = 1; i <= req.body.columns; i++) {
              for (var j = 1; j <= req.body.rows; j++) {
                seats.append({
                  column: i,
                  row: j
                })
              }
            }
            async.map(seats, function(rawSeat, callback) {
              Seat.create({
                row: rawSeat.row,
                column: rawSeat.column
              }).exec(function(err, seat) {
                seat.location = location;
                seat.save(function() {
                  if (err == null) {
                    callback("Error creating seats, may have been only partially done");
                  } else {
                    callback();
                  }
                })
              })
            }, function(err) {
              if (err == null) {
                return res.json(location);
              } else {
                return res.json({
                  error: "Seats were not successfully created, they may have been added incompletely"
                });
              }
            });
          });
          return res.json(location);
        }
      };
    });
  },

  deleteLocation: function(req, res, next) {
    Location.findOne(req.param('id'), function foundLocation(err, location) {
      if (err) return next(err);

      if (!location) return next('Location doesn\'t exist.');

      Location.deleteLocation(req.param('id'), function locationDestroyed(err) {
        if (err) return next(err);
      });
    });
  }

};
