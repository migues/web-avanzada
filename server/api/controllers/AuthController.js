/**
 * AuthController
 *
 * @module      :: Controller
 * @description	:: Provides the base authentication
 *                 actions used to make auth work.
 *
 */

var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var secret = "hkas3dir3k4s9cl340";

// Creates a new json web token with the user id
var createToken = function(id) {
  return jwt.sign({
    user: id
  }, secret, {
    expiresIn: 24 * 60 * 60 * 30 // expires in 30 days
  });
};

module.exports = {

  login: function(req,res) {
    if (req.body.email == undefined || req.body.password == undefined) {
      return res.json({error: "incorrect username or pass"});
    }

    User.findOne().where({email: req.body.email}).exec(function(err, user) {
      if (user == null) {
        return res.json({
          error: "incorrect pass"
        });
      }
      else if (err == null) {
      bcrypt.compare(req.body.password, user.password, function(err, result) {
        if (result == true) {
          var token = createToken(user.id);
          return res.json({
            token: token
          });
        } else {
          return res.json({error: "incorrect username or pass"});
        }
      });
    } else {
      return res.json({error: "incorrect username or pass"});
    }
    });
  },

  register: function(req, res) {
    if (req.body.password.length === 0) {
      return res.json({
        code: 1,
        message: 'A password must be specified'
      });
    }

    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(req.body.password, salt, function(err, hash) {
        var user = User.create({
          password: hash,
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
        }).exec(function(err, created) {
          if (err == null) {
            var token = createToken(created.id);
            return res.json({
              token: token
            });
          } else {
            return res.json({
              error: "email already exists."
            });
          }
        });
      });
    });
  }

};
