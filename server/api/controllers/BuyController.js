/**
 * BuyController
 *
 * @description :: Server-side logic for managing buys
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
	 addBuy: function(req, res, next) {

    var locObj = {
      amount: req.param('amount')
    }

    
    Buy.create(locObj, function buyCreated(err, buy) {

      if (err) {
        console.log(err);
        req.session.flash = {
          err: err
        }

      }
      
    });
  },

   deleteBuy: function(req, res, next) {

    Buy.findOne(req.param('id'), function foundBuy(err, buy) {
      if (err) return next(err);

      if (!buy) return next('Buy doesn\'t exist.');

      Buy.deleteBuy(req.param('id'), function buyDestroyed(err) {
        if (err) return next(err);

      });        


    });
  }

};

