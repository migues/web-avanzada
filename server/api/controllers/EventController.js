/**
 * EventController
 *
 * @description :: Server-side logic for managing events
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  /**
   * `EventController.cancel()`
   */
  cancel: function(req, res) {

    Event.findOne().where({
      id: req.params.id
    }).exec(function(err, result) {

      if (result == null) {
        return res.json({
          error: "Event not found"
        });
      } else {

        var eventFound = result;
        eventFound.isActive = false;

        eventFound.save(
          function(err, s) {
            console.log('The event: ' + s.name + ' has been canceled');
            return res.json(s);
          });
      }

    });
  },

  undoCancel: function(req, res) {

    Event.findOne().where({
      id: req.params.id
    }).exec(function(err, result) {

      if (result == null) {
        return res.json({
          error: "Event not found"
        });
      } else {

        var eventFound = result;
        eventFound.isActive = true;

        eventFound.save(
          function(err, s) {
            console.log('The event: ' + s.name + ' has been re-activated');
            return res.json(s);
          });
      }

    });
  },


  /**
   * `EventController.reschedule()`
   */
  reschedule: function(req, res) {
		if (req.body.id == null || req.body.date == null ) {
			return res.json({error: "incomplete body"});
		}

    Event.findOne(req.body.id).populate('tickets').exec(function(err, event) {
      if (err == null) {
        async.map(event.tickets, function(item, callback) {
          Ticket.findOne(item.id).populate('owner').exec(function(err, ticket) {
            callback(err, ticker.owner.user);
          })
        }, function(err, users) {
					if (err == null) {
						event.date = req.body.date;
						event.save(function(){
							mailService.send(users, "Cambio de horarios para un evento", "El evento '"+event.name+"' ha cambiado su fecha hacia el "+event.date);
							return res.json({message: "success"});
						});
					} else {
						return res.json({error: err});
					}
        });
      } else {
        return res.json({
          error: err
        });
      }
    });
  }
};
