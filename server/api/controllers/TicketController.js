/**
 * TicketController
 *
 * @description :: Server-side logic for managing Tickets
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

/**
Returns the amount bought via a callback
@param ticketId The ticket id
@param callback The callback, receives an int
*/
var boughtCount = function(ticketId, callback) {
  Ticket.findOne(ticketId).populate("bought").exec(function(err, ticket) {
    var count = 0
    for (var i = 0; i < ticket.bought.length; i++) {
      var current = ticket.bought[i]
      count += current.amount
    }
    callback(amount);
  });
}

module.exports = {
  create: function(req, res) {
    if (validator.validate(["name", "price", "amount", "event"], req.body) == false) {
      return res.json({
        code: 1,
        error: "Incomplete request, must have a name, price, amount and event field"
      })
    }

    var name = req.body.name;
    var price = req.body.price;
    var amount = req.body.amount;
    var eventId = req.body.eventId;
    var description = req.body.desc;
    var creatorId = req.userId;

    Ticket.create({
      name: name,
      price: price,
      amount: amount,
      description: description,
      owner: creatorId
    }).exec(function(err, ticket) {
      if (err == null) {
        Event.findOne(eventId).exec(function(err, event) {
          if (err == null) {
            ticket.event = event;
            ticket.save(function() {
              return res.json(ticket);
            })
          } else {
            return res.json(err);
          }
        });
      } else {
        return res.json(err);
      }
    })
  },

  /**
  Buys a ticket for the current user
  */
  buy: function(req, res) {
    var ticketId = req.body.ticketId;
    var userId = req.userId;
    var amount = req.body.amount;

    var column = req.body.column;
    var row = req.body.row;

    if (row == null || column == null) {
      row = -1;
      column = -1;
    }

    if (amount == null) {
      amount = 1;
    }
    
    if (userId == null || ticketId == null) {
      return res.json({
        error: "Must specify a user and ticket"
      });
    } else {
      Ticket.findOne(ticketId).exec(function(err, ticket) {
        amountBought(ticketId, function(amountRemaining) {
          if (amountRemaining < amount) {
            return res.json({
              error: "Can't buy more tickets than what is available"
            });
          }
          User.findOne(userId).exec(function(err, user) {
            Event.findOne(ticket.event).populate("seats", {
              where: {
                column: column,
                row: row
              }
            }).exec(function(err, event) {
              if (event.seats.length == 0) {
                Seat.findOne({
                  where: {
                    column: column,
                    row: row
                  }
                }).exec(function(err, seat) {
                  var amountDue = ticket.price * amount;
                  paymentService.pay(amountDue, user, function(successfull) {
                    if (successfull) {
                      UserTicket.create({
                        user: user,
                        ticket: ticket,
                        amountBought: amount
                      }).exec(function(err, userTicket) {
                        if (seat != null) {
                          userTicket.seat = seat;
                          userTicket.save(function() {
                            return res.json(userTicket);
                          });
                        }
                        return res.json(userTicket);
                      });
                    } else {
                      return res.json({
                        error: "Couldn't process the payment"
                      });
                    }
                  });
                });
              } else {
                return res.json({
                  error: "Seat already bought"
                });
              }
            });
          });
        })
      });
    }
  },

  amountBought: function(req, res) {
    var ticket = req.param("ticket")
    if (ticket == null || ticket == undefined) {
      return res.json({
        error: "ticket not found"
      });
    }

    boughtCount(ticket, function(amount) {
      return res.json({
        amount: amount
      });
    });
  }
};
