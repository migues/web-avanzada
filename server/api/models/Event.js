/**
* Event.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    name : { type: 'string' },

    email : { type: 'string' },

    date : { type: 'date' },

    image : {type: 'string' },

    isActive : {
      type: 'boolean',
      defaultsTo: true
    },

    ticketType : { type: 'string'},

    tickets : {
      collection : 'ticket',
      via : 'event'
    },

    location : {
      model : 'location'
    }
  }
};
