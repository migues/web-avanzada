/**
 * Location.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {

    name: {
      type: 'string'
    },

    latitude: {
      type: 'float'
    },

    longitude: {
      type: 'float'
    },

    rows: {
      type: 'float'
    },

    columns: {
      type: 'float'
    },

    events: {
      collection: 'event',
      via: 'location'
    },

    seats: {
      collection: 'seat',
      via: 'location'
    }
  }
};
