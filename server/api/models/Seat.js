/**
* Seat.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    row: "integer",
    column: "integer",
    location: {
      model: 'location'
    },
    tickets: {
      collection : 'userticket',
      via : 'seat'
    }
  }
};
