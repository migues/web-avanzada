/**
 * User
 *
 * @module      :: Model
 * @description :: This is the base user model
 * @docs        :: http://waterlock.ninja/documentation
 */

 module.exports = {

  attributes: {
    email: {
      type: 'string',
      required: true,
      unique: true
    },

    password: {
      type: 'string',
      required: true
    },

    firstName : {
      type: 'string'
    },

    lastName : {
      type: 'string'
    },

    tickets: {
      collection: 'userticket',
      via: 'user'
    },
  }
};
