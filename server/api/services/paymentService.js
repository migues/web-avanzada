module.exports = {
  /**
  Uses an external API to process the payments. currently a placeholder

  @param amount The amount to be charged
  @param user The user who is buying
  @param callback Called when the payment has completed, has a "successfull" = true parameter iff the transaction succeeds.
  @note Currently a placeholder
  */
  pay: function(amount, user, callback) {
    console.log("User "+user.name+" payed "+amount+" successfully");
    callback(true);
  }
}
