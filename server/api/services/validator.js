module.exports = {
  /**
  Validates that all the fields are in the data dictionary

  @param fields The fields which the API is expecting
  @param data The data the API has received
  @returns true iff the data dictionary has all the required fields
  */
  validate: function(fields, data) {
    for (var i = 0; i < fields.length; i++) {
      if (data[fields[i]] == undefined) {
        return false;
      }
    }
    return true;
  }
}
