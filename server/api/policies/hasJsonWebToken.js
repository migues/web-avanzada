'use strict';
/* jshint unused:false */

/**
 * hasJsonWebToken
 *
 * @module      :: Policy
 * @description :: Assumes that your request has an jwt;
 *
 */
 
var jwt = require('jsonwebtoken');
module.exports = function(req, res, next) {
  // the second parameter must match the secret in AuthController
  jwt.verify(req.headers["x-auth-token"], "hkas3dir3k4s9cl340", function(err, decoded) {
    if (err !== null) {
      return res.json(err);
    } else {
      req.userId = decoded.user;
      next();
    }
  });
};
